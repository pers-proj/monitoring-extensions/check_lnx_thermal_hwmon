#!/usr/bin/env python3

from pathlib import Path
from typing import Any, List
from .bakery_api.v1 import FileGenerator, OS, Plugin, PluginConfig, register

def get_lnx_thermal_hwmon_files(conf: Any) -> FileGenerator:
    yield Plugin(base_os=OS.LINUX, source=Path("lnx_thermal_hwmon.sh"), interval=0) # 0 seconds = use default interval; optional parameter

register.bakery_plugin(
    name="lnx_thermal_hwmon",
    files_function=get_lnx_thermal_hwmon_files,
)
