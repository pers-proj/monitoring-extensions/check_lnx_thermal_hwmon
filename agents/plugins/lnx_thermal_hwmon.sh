#!/bin/sh

set -eu

echo "<<<lnx_thermal:sep(124)>>>"

for hwmon in /sys/class/hwmon/hwmon*; do
	for F in "${hwmon}"/temp*_input; do
		if ! [ -f "$F" ]; then
			continue
		fi
		sensor="${F##*/}"
		sensor_base="${F%_input}"

		label_file="${sensor_base}_label"
		if [ -f "$label_file" ]; then
			read -r label < "$label_file"
		else
			continue
		fi

		device="$(readlink "${hwmon}/device")"

		# Ignore thermal_zone originated hwmon, since that's covered by
		# by the stock agent
		case "$device" in
			../../thermal_zone*)
				continue
				;;
		esac

		if ! read -r F < "$F"; then
			continue
		fi

		read -r device_name < "${hwmon}/name"

		# name | mode | type (unused) | temperature
		line="${label} (${device_name})|-|${hwmon##*/}_$sensor|${F}"

		max_temp_file="${sensor_base}_max" # max temp (if present)
		if [ -f "$max_temp_file" ]; then
			read -r max_temp < "$max_temp_file"
			line="${line}|${max_temp}|critical"
		fi

		max_hyst_temp_file="${sensor_base}_max_hyst" # "warn" temp (if present)
		if [ -f "$max_hyst_temp_file" ]; then
			read -r max_hyst_temp < "$max_hyst_temp_file"
			line="${line}|${max_hyst_temp}|hot"
		fi

		echo "$line"
	done
done
echo "<<<>>>"
